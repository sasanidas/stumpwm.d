(in-package :stumpwm)

;;; Panic button
(defcommand fer/close-session () ()
  (when (y-or-n-p "Close session? ")
    (run-shell-command "kill -9 -1")))

(define-key *top-map* (kbd "C-M-ESC") "fer/close-session")

;;; Debugging utilities
;; (setf *debug-level* 3)
;; (redirect-all-output (data-dir-file "debug-output" "txt"))

;;; Attach keyboard

(defcommand attach-keyboard () ()
  (run-shell-command "xmodmap -e 'clear mod4'" t)
  (run-shell-command "xmodmap -e 'keycode 133 = F20'" t)
  (set-prefix-key (kbd "F20"))
  (message "Keyboard, reattached."))

(define-key *top-map* (kbd "XF86Launch7") "attach-keyboard")

(define-key *top-map* (kbd "XF86Launch7") "attach-keyboard")

(define-key *top-map* (kbd "C-M-1") "attach-keyboard")

;;; Make stumpwm handle errors better
(setf *top-level-error-action* :break)



;(ql:quickload :slynk)
;(require :slynk)

;(defcommand slynk (&optional (port 4005)) (:number)
;	    (slynk:create-server :port port :dont-close t))

;;; Load micros (:url localhost :port 4005)

(ql:quickload :micros)
(require :micros)

(defcommand micros (&optional (port 4005)) (:number)
  (micros:create-server :port port :dont-close t))

;; Set super key as a prefix key
(run-shell-command "xmodmap -e 'clear mod4'" t)
(run-shell-command "xmodmap -e 'keycode 133 = F20'" t)
(set-prefix-key (kbd "F20"))

;;; General options
(setf *input-window-gravity* :center
      *message-window-gravity* :center
      *message-window-padding* 10
      *mouse-focus-policy* :click
      *window-border-style* :none
      *new-frame-action* :empty
      *grab-pointer-character* 102
      *mode-line-position* :top
      *mode-line-pad-y* 4
      *window-format* "%m%n%s%c")

;;; Add the sterling (Libra) to the keysym list
(define-keysym-name "£" "sterling")
;;(define-keysym #xa3 "Libra")

;;; Modeline

;; (defcommand enable-all-mode-lines () ()
;;   "Enable all mode lines on the current screen."
;;   (loop for head in (screen-heads (current-screen))
;;         do (enable-mode-line (current-screen) head t)))

;; ;; Default mode-line colours
;; (setf *mode-line-background-color* "Black"
;;       *mode-line-border-color* "White")

;; (enable-all-mode-lines)

;; ;; CPU
;; (ql:quickload :cpu)
;; (setf cpu::*cpu-modeline-fmt* "%c (%f) ")

;; ;; Wifi (it requires wireless-tools in Debian)
;; (ql:quickload :wifi)

;; ;; RAM
;; (load-module "mem") ; This enable the %M format modifier

;; brightnessctl

;; Brightness
(defvar *max-brightness*
  (parse-integer
   (stumpwm:run-shell-command "brightnessctl m" t) :junk-allowed t))

(defun current-brightness ()
  (parse-integer
   (stumpwm:run-shell-command "brightnessctl g"  t) :junk-allowed t))

(defcommand get-brightness () ()
  ""
  (stumpwm:message "~a" (current-brightness)))

(defcommand increase-bright () ()
  (stumpwm:run-shell-command
   (format nil "brightnessctl s ~a" (+ (current-brightness) 30)) t))

(defcommand decrease-bright () ()
  (stumpwm:run-shell-command
   (format nil "brightnessctl s ~a" (- (current-brightness) 30)) t))

(define-key *root-map* (kbd ".") "increase-bright")
(define-key *root-map* (kbd ",") "decrease-bright")



;; ;; Battery
;; (load-module "battery-portable")

;; (defparameter *fer/separator* "^(:fg \"Red\")  |  ^(:fg \"#84B166\")")

;; (setf *screen-mode-line-format* (list "^(:fg \"#84B166\")"
;; 				      "[%n] %W"
;; 				      *fer/separator*
;; 				      '(:eval (cpu::cpu-modeline nil))
;; 				      *fer/separator*
;; 				      "%M"
;; 				      *fer/separator*
;; 				      '(:eval (wifi::fmt-wifi nil))
;; 				      *fer/separator*
;; 				      "%B"
;; 				      *fer/separator*
;; 				      "%V"
;; 				      *fer/separator*
;; 				      "^>^(:fg \"#84B166\")%d                "))

;; ;; System tray
;; (ql:quickload :stumptray)
;; (when (> (length (screen-heads (current-screen))) 1)
;;   (setf stumptray::*tray-head-selection-fn* #'second))

;; (stumptray::stumptray)

;;; Gap
;; (load-module "swm-gaps")

;; (ql:quickload :swm-gaps)
;; (setf swm-gaps::*inner-gaps-size* 12)
;; (setf swm-gaps::*outer-gaps-size* 24)
;; (run-commands "toggle-gaps")

;;; Run gnome-panel modeline
(run-shell-command "mons -s && sleep 2 && gnome-panel -r")

(run-shell-command "xsetroot")


;;; proton
(ql:quickload :stumpwm-proton)

;;; ssh menu
(ql:quickload :swm-ssh)

(setq swm-ssh:*swm-ssh-default-term* "alacritty")

;;TODO: Move screen functions to a separate repository
(defun screen-active-p ()
  (not (cl-ppcre:scan
	"^No Sockets found in *"
	(stumpwm:run-shell-command "screen -list" t))))

(defun send-screen-command (command &key screen-session
					 (new-window t))
  (let* ((command-prefix (or screen-session "screen -X"))
	 (command-window (or (and new-window "screen -X screen ") "")))
    ;;"`echo -ne '\015'`"
    (if (screen-active-p)
	(stumpwm:run-shell-command (format nil
					   "~a && ~a stuff \"~a\""
					   command-window
					   command-prefix
					   command))
	(stumpwm:message "No screen session active."))))

;; This is an unapologetic copy of the swm-ssh::swm-ssh-menu command
(defcommand screen-connect-ssh () ()
  "Send the connect command to an active screen session."
  (let ((entry (stumpwm:select-from-menu
                (stumpwm:current-screen)
                (mapcar 'list (swm-ssh::collect-hosts))
                "Open ssh connection to: ")))
    (when entry
      (send-screen-command (format nil "ssh ~A" (car entry))))))


;;; Translate

;; (ql:quickload :stumpwm-translate)


;; ;; Translate
;; (defcommand auto-to-spanish () ()
;;   "Translate selected text to spanish."
;;   (stumpwm:message
;;    (stumpwm-translate:language-translate "auto"
;; 					 "es"
;; 					 (stumpwm:get-x-selection))))

;; (define-key *root-map* (kbd "t") "auto-to-spanish")

;; VPN
(ql:quickload :stumpwm-mullvad)

(define-key *root-map* (kbd "u") "mullvad-menu")


;; VPN
(ql:quickload :stumpwm-mullvad)

(define-key *root-map* (kbd "u") "mullvad-menu")




;;; Desktop entry
(ql:quickload :desktop-entry)

(desktop-entry:init-entry-list)

;;; Groups
(defvar *fer/groups* '("Programming" "Browser" "Aux"))

;; Configure main group
(grename "Main")

(loop for group in *fer/groups*
      do (gnewbg group))

;; Custom commands/functions


;; From https://github.com/jorams/dotfiles/
(defmacro fer/defkeymap (name &body bindings)
  (let ((map-sym (gensym)))
    `(defparameter ,name
       (let ((,map-sym (make-sparse-keymap)))
         ,@(loop for (key command) in bindings
                 append
		    (if (listp key)
			(loop for k in key
			      collect `(define-key ,map-sym (kbd ,k) ,command))
			(list `(define-key ,map-sym (kbd ,key) ,command))))
         ,map-sym))))

(defcommand fer/swap-windows () ()
  "Swap current window in a 2 layer group."
  (let ((frame-tree (tile-group-frame-tree (current-group)))
	(cwindow (current-window)) nwindow)
    (when (and (list frame-tree) (= (length (car frame-tree)) 2))
      (setf nwindow (frame-window
		     (second (car frame-tree)))))
    (if nwindow
	(progn
	  (if (string-equal (window-name
			     (frame-window
			      (first (car frame-tree))))
			    (window-name cwindow))
	      (fer/pull-window cwindow :cdir :right)
	      (fer/pull-window cwindow :cdir :left)))
	(print "This only works with 2 frame in the window tree."))))

(defun fer/pull-window (win1 &key cdir)
  (let ((odir (if (equal cdir :right) :left :right)))
    (progn
      (move-focus-and-or-window cdir nil)
      (move-focus-and-or-window odir t)
      (pull-window win1)
      (move-window cdir))))


(defun fer/get-group (group-number)
  (let ((sgroups (screen-groups (current-screen))))
    (find group-number sgroups :key #'group-number :test #'=)))

(defun fer/contains-frame-p (flist my-frame)
  (find my-frame flist :test #'equal))

(defcommand fer/hide-current-window () ()
  (hide-window (current-window)))

(defcommand fer/go-next-window () ()
  (let ((my-frame (tile-group-current-frame (current-group)))
	(frames-group (tile-group-frame-tree (current-group)))
	(cwindow (current-window)))
    (when cwindow
      (loop for fr in frames-group
	    when (and (atom fr)
		      (not (equal fr my-frame)))
	    return (pull-window cwindow fr)
	    when (and (listp fr) (not
				  (fer/contains-frame-p fr my-frame)))
	    return (pull-window cwindow (first fr))))))

(defcommand fer/select-group (group-number) ((:number "Select group number: "))
  (gselect (group-name (fer/get-group group-number))))

(defcommand fer/move-to-group (group-number) ((:number "Move to group number:"))
  (move-window-to-group (group-current-window (current-group))
			(fer/get-group group-number)))

(defmacro fer/defprogramsimple-shortcut (name &key (command (string-downcase (string name)))
						   (props `'(:class ,(string-capitalize command)))
						   (map '*top-map*)
						   (key `(kbd ,(concat "H-" (subseq command 0 1))))
						   (raise nil))
  "Simplify version of `defprogram-shortcut' that check if you want to create a new instance
of the program or just raise the program."
  (if raise
      `(progn
	 (defcommand ,name () ()
	   (run-or-raise ,command ,props))
	 (define-key ,map ,key ,(string-downcase (string name))))
      `(progn
	 (defcommand ,name () ()
	   (run-shell-command ,command))
	 (define-key ,map ,key ,(string-downcase (string name))))))

;;; Keys
;; Applications
(fer/defprogramsimple-shortcut
 terminal
 :key (kbd "RET")
 :map *root-map*
 :command "alacritty"
 :props '(:class "alacritty"))

(fer/defprogramsimple-shortcut
 firefox
 :key (kbd "b")
 :map *root-map*
 :command "firefox"
 :props '(:class "Firefox"))

(fer/defprogramsimple-shortcut
 pcmanfm
 :key (kbd "d")
 :map *root-map*
 :command "pcmanfm"
 :props '(:class "Pcmanfm"))

(fer/defprogramsimple-shortcut
 lem
 :key (kbd "l")
 :map *root-map*
 :command "lem"
 :raise t
 :props '(:class "lem"))
;; Dmenu

(ql:quickload :dmenu)

(define-key *root-map* (kbd "p") "dmenu-run")
(define-key *root-map* (kbd "w") "dmenu-windowlist")
;;(define-key *root-map* (kbd ";") "dmenu-call-command")

(define-key *root-map* (kbd "c") "proton-select")

(define-key *root-map* (kbd "j") "show-desktop-menu")

;; Screen/Window management
(define-key *root-map* (kbd "v") "hsplit")
(define-key *root-map* (kbd "h") "vsplit")
(define-key *root-map* (kbd "q") "delete-window")
(define-key *root-map* (kbd "k") "remove-split")
(define-key *root-map* (kbd "m") "fullscreen")
(define-key *root-map* (kbd "o") "fer/go-next-window")
(define-key *root-map* (kbd "@") "pull-marked")
(define-key *root-map* (kbd "n") "fer/hide-current-window")
(define-key *root-map* (kbd "C-s") "fer/swap-windows")

;; Window movement


;; Group movement
(define-key *root-map* (kbd "1") "fer/select-group 1")
(define-key *root-map* (kbd "2") "fer/select-group 2")
(define-key *root-map* (kbd "3") "fer/select-group 3")
(define-key *root-map* (kbd "4") "fer/select-group 4")

(define-key *root-map* (kbd "!")  "fer/move-to-group 1")
(define-key *root-map* (kbd "\"") "fer/move-to-group 2")
(define-key *root-map* (kbd "£")  "fer/move-to-group 3")
(define-key *root-map* (kbd "$")  "fer/move-to-group 4")


;; Logout
(define-key *root-map* (kbd "M-ESC") "run-shell-command kill -9 -1")

;; Shutdown

(defcommand fer/shutdown () ()
  (when (y-or-n-p "Shutdown? ")
    (run-shell-command "systemctl poweroff")))

(define-key *root-map* (kbd "C-ESC")  "fer/shutdown")
